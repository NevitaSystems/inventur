#!/usr/bin/python3
# -*- coding: iso-8859-1 -*-

__name__ = "Alexander Pokraka"
__email__ = "pokraka@th-brandenburg.de"
__version__ = "2.1"

#Python 3 tkinter, Python2 Tkinter
from Scripts import NeueKategorie, NeuesGer�t, Katergorie�ndern, Ger�t�ndern
from tkinter import ttk
from tkinter import *

import threading
import sqlite3


class Interface():
    def __init__(self):
        ###Datenbank
        #Datenbankverbindung
        connection = sqlite3.connect("inventur.db")
        cursor = connection.cursor()


        ###Fenster
        #Hauptfenster erstellen
        self.fe1 = Tk()
        self.fe1.title("Inventur")
        self.fe1.minsize(width=700, height=850)
        self.fe1.maxsize(width=700, height=850)

        #Fenster f�r die neue Kategorie und das neue Ger�t
        #siehe Kapitel: Button

        ###Frame
        #oberes Frame f�r die �berschirft
        self.f1 = Frame(self.fe1)
        self.f1.grid(row=0, column=0, columnspan=2)

        #mittleres linkes Frame f�r die Datenbankerg�nzung
        self.f2 = Frame(self.fe1)
        self.f2.grid(row=1, column=0)

        #mittleres rechte Frame f�r die Datenbankabfrage
        self.f3 = Frame(self.fe1)
        self.f3.grid(row=1, column=1)

        #Frame f�r die Datenbankinformationsabfrage
        self.f4 = Frame(self.fe1)
        self.f4.grid(row=2, column=0, columnspan=2)

        #Frame f�r den Exit-Button
        self.f5 = Frame(self.fe1)
        self.f5.grid(row=3, column=0, columnspan=2)

        ###Label
        #Label f�r die �beschrift
        self.l1 = Label(self.f1, text="Inventarisierungsprogramm", font = "Helvetica 16 bold italic")
        self.l1.grid(row=0, column=1, pady=15)

        #Label f�r die Button
        self.l2 = Label(self.f2, text="Mit den unteren Button k�nnen neue \n"
                            "Kategorie und Ger�te in der Datenbank \n"
                            "angelegt, ge�ndert oder gel�scht werden.")
        self.l2.grid(row=1, column=0, columnspan=2, padx=15, pady=15)

        #Label f�r die Ausgabe der SQLite-Abfrage
        self.l3 = Label(self.f3, text="Abfrage aus der SQLite-Datenbank.")
        self.l3.grid(row=1, column=2, columnspan=2, pady=30)


        ###Button
        ##Buttondefinieren
        #Button f�r neue Kategorie
        def ButNeuKat():
            NeueKategorie.NeueKategorie(self.fe1)

        #Button f�r das neue Ger�t
        def ButNeuGer():
            NeuesGer�t.NeuesGer�t(self.fe1)


        #Button das �ndern einer Kategorie
        def ButAenKat():
            Katergorie�ndern.�nderungKategorie(self.fe1)


        #Button f�r das �ndern eine Ger�tes
        def ButAenGer():
            Ger�t�ndern.�nderungGer�te(self.fe1)


        #Button f�r die Kategorie aus der SQlite �ber Tree
        def ButInfKat2():
            self.scrButInfKat1 = Scrollbar(self.f4)
            self.scrButInfKat1.grid(row=0, column=1, sticky=N + S, pady=15)
            self.tree1 = ttk.Treeview(self.f4)
            self.tree1["columns"] = ("Kategorie")
            self.tree1.column("#0", width=0)
            self.tree1.column("Kategorie", width=600)
            self.tree1.heading("Kategorie", text="Kategorie")
            self.tree1.grid(row=0, column=0, sticky=N + S + E + W, padx=15, pady=15)

            self.InfKatSQL1 = cursor.execute("SELECT k.Name, k.ID FROM Kategorie k")
            for self.line in self.InfKatSQL1:
                self.tree1.insert("", 0, values=(str(self.line[0]), str(self.line[1])))
            self.scrButInfKat1.config(command=self.tree1.yview)

        #Button f�r die Ger�te aus der SQLite �ber Tree
        def ButInfGer2():
            # Tree
            self.tree2 = ttk.Treeview(self.f4)
            self.tree2["columns"]=("Kategorie", "Name", "Anzahl", "Anmerkung")
            self.tree2.column("#0", width=0)
            self.tree2.column("Kategorie", width=135)
            self.tree2.column("Name", width=154)
            self.tree2.column("Anzahl", width=50)
            self.tree2.column("Anmerkung", width=280)
            self.tree2.heading("Kategorie", text="Kategorie")
            self.tree2.heading("Name", text="Name")
            self.tree2.heading("Anzahl", text="Anzahl")
            self.tree2.heading("Anmerkung", text="Anmerkung")
            self.tree2.grid(row=0, column=0, sticky=N + S + E + W, padx=15, pady=15)

            self.InfGerSQL1 = cursor.execute("SELECT k.Name, g.Name, g.Anzahl, g.Anmerkung FROM Kategorie k, Ger�te g WHERE g.F_ID_Kategorie = k.ID")
            for self.line in self.InfGerSQL1:
                self.tree2.insert("", 0, values=(str(self.line[0]), str(self.line[1]), str(self.line[2]), str(self.line[3])))

            self.scrButInfGer1 = Scrollbar(self.f4, orient=VERTICAL, command=self.tree2.yview)
            self.scrButInfGer1.grid(row=0, column=1, sticky=N + S, pady=15)

        ##Button im Hauptfenster
        #Button f�r neue Kategorien
        self.b1 = Button(self.f2, text="neue Kategorie anlegen", command=ButNeuKat, width=20, height=2)
        self.b1.grid(row=2, column=0, padx=15)

        #Button f�r neue Ger�te
        self.b2 = Button(self.f2, text="neues Ger�te anlegen", command=ButNeuGer, width=20, height=2)
        self.b2.grid(row=2, column=1)

        #Button um eine Kategorie zu �ndern
        self.b3 = Button(self.f2, text="Kategorie �ndern", command=ButAenKat, width=20, height=2)
        self.b3.grid(row=3, column=0, pady=2)

        #Button um ein Ger�t zu �ndern
        self.b4 = Button(self.f2, text="Ger�t �ndern/l�schen", command=ButAenGer, width=20, height=2)
        self.b4.grid(row=3, column=1, pady=2)

        #Button, um die Kategorien aus der Datenbank abzufragen
        self.b5 = Button(self.f3, text="Kategorien", command=ButInfKat2, width=20, height=5)
        self.b5.grid(row=2, column=2, padx=15)

        #Button, um die Ger�te aus der Datenbank abzufragen
        self.b6 = Button(self.f3, text="Ger�te", command=ButInfGer2, width=20, height=5)
        self.b6.grid(row=2, column=3)

        #Button, um das Programm zu schlie�en
        self.f7 = Button(self.f5, text="Schlie�en", command=self.fe1.quit, height=2, width=50, pady=25)
        self.f7.grid(row=0, column=1)


        ###Listbox
        #Listbox mit Scrollbar f�r die Datenbankabfrage
        self.sc1 = Scrollbar(self.f4, orient=VERTICAL)
        self.sc1.grid(row=0, column=1, sticky=N+S, pady =15)

        self.lb1 = Listbox(self.f4, yscrollcommand=self.sc1.set)
        self.lb1.grid(row=0, column=0, sticky=N+S+E+W, padx=15, pady=15, ipadx=250, ipady=150)

        #Hauptschleife
        self.fe1.mainloop()

if __name__ == '__main__':
    app = Interface()
    app.mainloop()