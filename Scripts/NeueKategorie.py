#!/usr/bin/python3
# -*- coding: iso-8859-1 -*-

#Python 3 tkinter, Python2 Tkinter
from tkinter import *
from tkinter import ttk
import sqlite3

###Datenbank
#Datenbankverbindung
connection = sqlite3.connect("inventur_v2.db")
cursor = connection.cursor()

def NeueKategorie(fe2):

    x = 350
    y = 100

    # neues Fenster
    fe2 = Toplevel()
    fe2.title("neue Kategorie hinzuf�gen")
    fe2.minsize(width=x, height=y)
    fe2.maxsize(width=x, height=y)

    # Frame f�r den Namen
    fButNeuKat1 = Frame(fe2)
    fButNeuKat1.grid(row=0)

    # Frame f�r die Button
    fButNeuKat2 = Frame(fe2)
    fButNeuKat2.grid(row=1)

    # Text mit Eingabefeld
    lButNeuKat1 = Label(fButNeuKat1, text="Name:")
    lButNeuKat1.grid(row=0, column=0, padx=10, pady=5)
    eButNeuKat1 = Entry(fButNeuKat1, width=45)
    eButNeuKat1.grid(row=0, column=1)

    def BesButNeuKat():
        cursor.execute("INSERT INTO Kategorie (Name) VALUES (?)", (eButNeuKat1.get(),))
        connection.commit()
        fe2.destroy()

    # Button Speichern
    bButNeuKat2 = Button(fButNeuKat2, text="Speichern", command=BesButNeuKat, width=10, height=2)
    bButNeuKat2.grid(row=0, column=0, padx=5, pady=5)

    # Button Exit
    bButNeuKat1 = Button(fButNeuKat2, text="Abbruch", command=fe2.destroy, width=10, height=2)
    bButNeuKat1.grid(row=0, column=1, padx=5)