#!/usr/bin/python3
# -*- coding: iso-8859-1 -*-

#Python 3 tkinter, Python2 Tkinter
from tkinter import *
from tkinter import ttk
import sqlite3
from Scripts import NeueKategorie

###Datenbank
#Datenbankverbindung
connection = sqlite3.connect("inventur_v2.db")
cursor = connection.cursor()

def NeuesGer�t(fe3):

    x = 500
    y = 275

    # neues Fenster
    fe3 = Toplevel()
    fe3.title("neues Ger�t hinzuf�gen")
    fe3.minsize(width=x, height=y)
    fe3.maxsize(width=x, height=y)

    # Frame f�r den Namen
    fButNeuGer1 = Frame(fe3)
    fButNeuGer1.grid(row=0)

    # Frame f�r die Button
    fButNeuGer2 = Frame(fe3)
    fButNeuGer2.grid(row=1)

    # Scrollbar mit Listbox f�r die Kategorie
    lButNeuGer1 = Label(fButNeuGer1, text="Kategorie:")
    lButNeuGer1.grid(row=0, column=0, pady=5)
    scrButNeuGer1 = Scrollbar(fButNeuGer1)
    scrButNeuGer1.grid(row=0, column=2, sticky=N + S)
    lbButNeuGer1 = Listbox(fButNeuGer1, yscrollcommand=scrButNeuGer1.set, selectmode=SINGLE, width=40, height=5)
    lbButNeuGer1.grid(row=0, column=1)
    InfGerSQL1 = cursor.execute("SELECT k.Name FROM Kategorie k")
    for line in InfGerSQL1:
        lbButNeuGer1.insert(END, str(line[0]))
    scrButNeuGer1.config(command=lbButNeuGer1.yview)

    # ID der Kategorie
    lButNeuGer2 = Label(fButNeuGer1, text="ID der Kategorie:")
    lButNeuGer2.grid(row=2, column=0, padx=10, pady=5)
    eButNeuGer2 = Entry(fButNeuGer1, width=40)
    eButNeuGer2.grid(row=2, column=1)

    # Name
    lButNeuGer3 = Label(fButNeuGer1, text="Name:")
    lButNeuGer3.grid(row=3, column=0, padx=10, pady=5)
    eButNeuGer3 = Entry(fButNeuGer1, width=40)
    eButNeuGer3.grid(row=3, column=1)

    # Anzahl
    lButNeuGer4 = Label(fButNeuGer1, text="Anzahl:")
    lButNeuGer4.grid(row=4, column=0, padx=10, pady=5)
    eButNeuGer4 = Entry(fButNeuGer1, width=40)
    eButNeuGer4.grid(row=4, column=1)

    # Anmerkung
    lButNeuGer5 = Label(fButNeuGer1, text="Anmerkung:")
    lButNeuGer5.grid(row=5, column=0, padx=10, pady=5)
    eButNeuGer5 = Entry(fButNeuGer1, width=40)
    eButNeuGer5.grid(row=5, column=1)

    def BesAktNeuGer():
        eButNeuGer2.delete(0, END)
        ActMak1 = lbButNeuGer1.get(ACTIVE)
        SQL1 = cursor.execute("SELECT k.ID FROM Kategorie k WHERE k.Name = ?", (ActMak1,))
        for line in SQL1:
            eButNeuGer2.insert(END, line[0])

    def BesButNeuGer():
        cursor.execute("INSERT INTO Ger�te (Name, Anzahl, Anmerkung, F_ID_Kategorie) VALUES(?,?,?,?)",
                       (eButNeuGer3.get(), eButNeuGer4.get(), eButNeuGer5.get(), eButNeuGer2.get(),))
        connection.commit()
        fe3.destroy()

    def BesAktNeuKat():
        NeueKategorie.NeueKategorie(fe3)

    # Button Speichern
    bButNeuGer1 = Button(fButNeuGer2, text="Speichern", command=BesButNeuGer, width=15, height=2)
    bButNeuGer1.grid(row=0, column=0, padx=5, pady=5)

    # Button Aktualisieren
    bButNeuGer2 = Button(fButNeuGer2, text="Aktualisieren", command=BesAktNeuGer, width=15, height=2)
    bButNeuGer2.grid(row=0, column=1, padx=5)

    # Neue Kategorie anlegen
    bButNeuGer3 = Button(fButNeuGer2, text="neue Kategorie", command=BesAktNeuKat, width=15, height=2)
    bButNeuGer3.grid(row=0, column=2, padx=5)

    # Button Exitdb
    bButNeuGer4 = Button(fButNeuGer2, text="Abbruch", command=fe3.destroy, width=15, height=2)
    bButNeuGer4.grid(row=0, column=3, padx=5)