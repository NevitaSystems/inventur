#!/usr/bin/python3
# -*- coding: iso-8859-1 -*-

#Python 3 tkinter, Python2 Tkinter
from tkinter import *
from tkinter import ttk
import sqlite3

###Datenbank
#Datenbankverbindung
connection = sqlite3.connect("inventur_v2.db")
cursor = connection.cursor()

def �nderungKategorie(fe4):

    x = 375
    y = 175

    # neues Fenster
    fe4 = Toplevel()
    fe4.title("Kategorie �ndern")# oder l�schen")
    fe4.minsize(width=x, height=y)
    fe4.maxsize(width=x, height=y)

    # Frame f�r den Namen
    fButAenKat1 = Frame(fe4)
    fButAenKat1.grid(row=0)

    # Frame f�r die Button
    fButAenKat2 = Frame(fe4)
    fButAenKat2.grid(row=1, column=0)

    # Scrollbar mit Listbox f�r die alte Kategorie
    lButAltKat1 = Label(fButAenKat1, text="Kategorie:")
    lButAltKat1.grid(row=0, column=0, pady=5)
    scrButAenKat1 = Scrollbar(fButAenKat1)
    scrButAenKat1.grid(row=0, column=2, sticky=N + S)
    lbButAenKat1 = Listbox(fButAenKat1, yscrollcommand=scrButAenKat1.set, selectmode=SINGLE, width=40, height=5)
    lbButAenKat1.grid(row=0, column=1, pady=5)
    InfGerSQL1 = cursor.execute("SELECT Name FROM Kategorie")
    for line in InfGerSQL1:
        lbButAenKat1.insert(END, str(line[0]))
    scrButAenKat1.config(command=lbButAenKat1.yview)

    #Label und Eingabefeld f�r die neue Kategorie
    lButAenKat2 = Label(fButAenKat1, text="neue Kategorie")
    lButAenKat2.grid(row=1, column=0, padx=5)
    eButAenKat2 = Entry(fButAenKat1, width=40)
    eButAenKat2.grid(row=1, column=1)

    def ButBesKatAen():
        cursor.execute("UPDATE Kategorie SET Name = ? WHERE Name = ?", (eButAenKat2.get(), lbButAenKat1.get(ACTIVE), ))
        connection.commit()
        fe4.destroy()

    def ButBesKatLoe():
        cursor.execute("DELETE FROM Kategorie WHERE Name=?" , (lbButAenKat1.get(ACTIVE), ))
        connection.commit()
        fe4.destroy()


    # Button Speichern
    bButAenKat1 = Button(fButAenKat2, text="Speichern", command=ButBesKatAen, width=10, height=2)
    bButAenKat1.grid(row=0, column=0, padx=5, pady=10)

    # Button L�schen
    #bButAenKat2 = Button(fButAenKat2, text="L�schen", command=ButBesKatLoe, width=10, height=2)
    #bButAenKat2.grid(row=0, column=1, padx=2)

    # Button Abbruch
    bButAenKat3 = Button(fButAenKat2, text="Abbruch", command=fe4.destroy, width=10, height=2)
    bButAenKat3.grid(row=0, column=2, padx=2)