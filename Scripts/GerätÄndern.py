#!/usr/bin/python3
# -*- coding: iso-8859-1 -*-

#Python 3 tkinter, Python2 Tkinter
from tkinter import *
from tkinter import ttk
import sqlite3

###Datenbank
#Datenbankverbindung
connection = sqlite3.connect("inventur_v2.db")
cursor = connection.cursor()

def �nderungGer�te(fe5):

    x = 625
    y = 350

    # neues Fenster f�r die �nderungen
    fe5 = Toplevel()
    fe5.title("Ger�t �ndern oder l�schen")
    fe5.minsize(width=x, height=y)
    fe5.maxsize(width=x, height=y)

    # Frame f�r den Namen
    fButAenGer1 = Frame(fe5)
    fButAenGer1.grid(row=0, column=0)

    # Frame f�r die Button
    fButAenGer2 = Frame(fe5)
    fButAenGer2.grid(row=1, column=0)

    # Frame f�r die Liste der ID und Namen der Kategorie
    fButAenGer3 = Frame(fe5)
    fButAenGer3.grid(row=0, column=1)

    # Scrollbar mit Listbox f�r das alte Ger�t
    lButAltGer1 = Label(fButAenGer1, text="Ger�t:")
    lButAltGer1.grid(row=0, column=0, pady=5)
    scrButAenGer1 = Scrollbar(fButAenGer1, orient=VERTICAL)
    scrButAenGer1.grid(row=0, column=2, sticky=N + S)
    #scrButAenGer2 = Scrollbar(fButAenGer1, orient=HORIZONTAL)
    #scrButAenGer2.grid(row=1, column=1, columnspan=2,  sticky=W + E)
    lbButAenGer1 = Listbox(fButAenGer1, yscrollcommand=scrButAenGer1.set, selectmode=SINGLE, width=40, height=5)
    lbButAenGer1.grid(row=0, column=1)
    InfAenGerSQL1 = cursor.execute("SELECT g.Name FROM Kategorie k, Ger�te g WHERE g.F_ID_Kategorie = k.ID")
    for line in InfAenGerSQL1:
        lbButAenGer1.insert(END, str(line[0]))
    scrButAenGer1.config(command=lbButAenGer1.yview)

    # Label und Eingabefeld f�r die ID der Kategorie
    lButAenGer1 = Label(fButAenGer1, text="ID:")
    lButAenGer1.grid(row=2, column=0, padx=5, pady=5)
    eButAenGer1 = Entry(fButAenGer1, width=40)
    eButAenGer1.grid(row=2, column=1)

    # Label und Eingabefeld f�r das neue Ger�t: Name
    lButAenGer2 = Label(fButAenGer1, text="Name:")
    lButAenGer2.grid(row=3, column=0, padx=5, pady=5)
    eButAenGer2 = Entry(fButAenGer1, width=40)
    eButAenGer2.grid(row=3, column=1)

    # Label und Eingabefeld f�r das neue Ger�t: Anzahl
    lButAenGer3 = Label(fButAenGer1, text="Anzahl:")
    lButAenGer3.grid(row=4, column=0, padx=5, pady=5)
    eButAenGer3 = Entry(fButAenGer1, width=40)
    eButAenGer3.grid(row=4, column=1)

    # Label und Eingabefeld f�r das neue Ger�t: Anmerkung
    lButAenGer4 = Label(fButAenGer1, text="Anmerkung:")
    lButAenGer4.grid(row=5, column=0, padx=5, pady=5)
    eButAenGer4 = Entry(fButAenGer1, width=40)
    eButAenGer4.grid(row=5, column=1)

    # SQL-Abfrage f�r die Kategorie
    tree3 = ttk.Treeview(fButAenGer3)
    tree3["columns"] = ("ID", "Kategoriename")
    tree3.column("#0", width=0)
    tree3.column("ID", width=30)
    tree3.column("Kategoriename", width=225)
    tree3.heading("ID", text="ID")
    tree3.heading("Kategoriename", text="Kategoriename")
    tree3.grid(row=0, column=3,sticky=N + S + E + W, pady=25)
    NameZuIDSQL1 = cursor.execute("SELECT k.ID, k.Name FROM Kategorie k")
    for line in NameZuIDSQL1:
        tree3.insert("", 0, values=(int(line[0]), str(line[1])))


    ##Zwischenspeicher
    # Label und Eingabefeld f�r das neue Ger�t: ID der Kategorie
    eButAenGer5 = Entry(fButAenGer1)

    # Label und Eingabefeld f�r das neue Ger�t: Name
    eButAenGer6 = Entry(fButAenGer1)

    # Label und Eingabefeld f�r das neue Ger�t: Anzahl
    eButAenGer7 = Entry(fButAenGer1)

    # Label und Eingabefeld f�r das neue Ger�t: Anmerkung
    eButAenGer8 = Entry(fButAenGer1)

    # Eingabefeld f�r die ID
    eButAenGer9 = Entry(fButAenGer1)


    def ButBesGerAkt():
        # L�schung der Eingabefelder
        eButAenGer1.delete(0, END)
        eButAenGer2.delete(0, END)
        eButAenGer3.delete(0, END)
        eButAenGer4.delete(0, END)
        eButAenGer5.delete(0, END)
        eButAenGer6.delete(0, END)
        eButAenGer7.delete(0, END)
        eButAenGer8.delete(0, END)
        eButAenGer9.delete(0, END)

        ActMak2 = lbButAenGer1.get(ACTIVE)

        # SQL-Umwandlung: Name in ID
        NameZuIDSQL2 = cursor.execute("SELECT g.ID FROM Ger�te g WHERE g.Name=?", (ActMak2,))
        for line in NameZuIDSQL2:
            eButAenGer9.insert(END, line[0])

        # Name ins Eingabefeld einf�gen
        SQL2 = cursor.execute("SELECT g.F_ID_Kategorie FROM Ger�te g WHERE g.ID = ?", (eButAenGer9.get(),))
        for line in SQL2:
            eButAenGer1.insert(END, line[0])
            eButAenGer5.insert(END, line[0])

        # Name ins Eingabefeld einf�gen
        SQL4 = cursor.execute("SELECT g.Name FROM Ger�te g WHERE g.ID = ?", (eButAenGer9.get(),))
        for line in SQL4:
            eButAenGer2.insert(END, line[0])
            eButAenGer6.insert(END, line[0])

        # Anzahl ins Eingabefeld einf�gen
        SQL6 = cursor.execute("SELECT g.Anzahl FROM Ger�te g WHERE g.ID = ?", (eButAenGer9.get(),))
        for line in SQL6:
            eButAenGer3.insert(END, line[0])
            eButAenGer7.insert(END, line[0])

        # Anmerkung ins Eingabefeld einf�gen
        SQL8 = cursor.execute("SELECT g.Anmerkung FROM Ger�te g WHERE g.ID = ?", (eButAenGer9.get(),))
        for line in SQL8:
            eButAenGer4.insert(END, line[0])
            eButAenGer8.insert(END, line[0])

    def ButBesGerAen():
        cursor.execute("UPDATE Ger�te SET F_ID_Kategorie=? WHERE ID=?", (eButAenGer1.get(), eButAenGer9.get(), ))
        cursor.execute("UPDATE Ger�te SET Name=? WHERE ID=?", (eButAenGer2.get(), eButAenGer9.get(), ))
        cursor.execute("UPDATE Ger�te SET Anzahl=? WHERE ID=?", (eButAenGer3.get(), eButAenGer9.get(),))
        cursor.execute("UPDATE Ger�te SET Anmerkung=? WHERE ID=?", (eButAenGer4.get(), eButAenGer9.get(),))
        connection.commit()
        fe5.destroy()

    def ButBesGerLoe():
        cursor.execute("DELETE FROM Ger�te WHERE Name=? ", (lbButAenGer1.get(ACTIVE), ))
        connection.commit()
        fe5.destroy()

    # Button Speichern
    bButAenGer1 = Button(fButAenGer2, text="Speichern", command=ButBesGerAen, width=10, height=2)
    bButAenGer1.grid(row=0, column=1, padx=5, pady=10)

    #Button Aktualisieren
    bButAenGer2 = Button(fButAenGer2, text="Aktualisieren", command=ButBesGerAkt, width=10, height=2)
    bButAenGer2.grid(row=0, column=2, padx=2)

    # Button L�schen
    bButAenGer3 = Button(fButAenGer2, text="L�schen", command=ButBesGerLoe, width=10, height=2)
    bButAenGer3.grid(row=0, column=3, padx=2)

    # Button Exit
    bButAenGer4 = Button(fButAenGer2, text="Abbruch", command=fe5.destroy, width=10, height=2)
    bButAenGer4.grid(row=0, column=4, padx=5)